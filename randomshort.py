#!/usr/bin/python3


import webapp
from urllib import parse
import shelve
import random
import os
import string
#pagina principal
MAIN_PAGE = """
<html>
    <body>
        <h1>Bienvenido a Randomshort por Adrian Cañizares</h1>
        <form action="/" method="POST">
            <div>
                <label> Url para recortar: </label>
                <input type="text" name="url" required>
            </div>
            <div>
                <input type="submit" value="SHORT">
            </div>
        </form>
        <h2> Url recortada: </h2>
        <pre>{current}</pre>
        <h3> Enlaces cortados: </h3>
        <pre>{dict}</pre>
    </body>
</html>
"""
# Pagina de error 405 Method not allowed
PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Recurso no permitido: {method}.</p>
  </body>
</html>
"""

# Pagina de error 404 Not found
PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Recurso no encontrado</h1>
  </body>
</html>
"""
class URLShortener(webapp.webApp):
    def __init__(self, hostname, port):
        self.data = {}
        super().__init__(hostname, port)
    def parse(self, request):
        data = {}  #Almacenamos recurso
        body_start = request.find('\r\n\r\n') # Nos quedamos ultima pos (cuerpo)
        if body_start == -1:
            # No hay cuerpo
            data['body'] = None
        else:
            # Hay cuerpo
            data['body'] = request[body_start + 4:]
        #dividimos en tres partes(meth, recurso y cuerpo)
        parts = request.split(' ')
        data['resource'] = parts[1]
        data['method'] = parts[0]
        return data

    def read_dict(self):
        if os.path.isfile('dict.db'):
            with shelve.open('dict.db', 'r') as db:
                self.dict = db['dict']
        else:
            with shelve.open('dict.db', 'c') as db:
                db['dict'] = self.dict
        db.close()

    def process(self, data): #procesamos peticion y devolvemos pagina y el codigo.
        if data['method'] == 'GET':#en caso que sea get hace cosas
            code, page = self.get(data['resource'])
        elif data['method'] == 'POST':#caso post
            code, page = self.post(data['body'])
        else:
            code = "405 Method not allowed"
            page = PAGE_NOT_ALLOWED.format(method=data['method'])
        return code, page

    def get_key(self, value):
        # Devuelve  llave del valor en el diccionario
        for key, val in self.dict.items():
            if val == value:
                return key

    def get(self, resource, current):
        if resource == '/':
            code = "200 OK"
            #dicionario con urls acortadas
            dict = "\n".join([f'Recortada: http://localhost:{self.port}{key} '
                              f'--- Real: {value}' for key, value in self.dict.items()])
            #pagina con principal urls acortadas ukltima y anteriores
            page = MAIN_PAGE.format(dict=dict, current=current)
        elif resource in self.dict.keys(): #si esta en el dicionario hacemos redireccion
            code = f"307 Temporary Redirect\r\nLocation: {self.dict[resource]}"
            page = '<html></html>'
        else:
            code = "404 Not Found"
            page = PAGE_NOT_FOUND.format(resource=resource)
        return code, page

    def post(self, body):
        fields = parse.parse_qs(body)  #diccionario con la URL introducida
        if 'url' in fields:  # Si el hay una URL en el cuerpo de la peticion
            url = fields['url'][0]
            # Si no empieza con http:// o https:// es https por defecto
            if not url.startswith('http://') and not url.startswith('https://'):
                url = 'https://' + url
            # Añadimos url si no esta en el dict
            if url not in self.dict.values():
                new_resource = str(random.choices(string.ascii_lowercase + string.digits, k=32))
                self.dict[new_resource] = url
                with shelve.open('dict.db', 'w') as db:
                    db['dict'] = self.dict
                db.close()
            # Guardo la URL que se acaba de recortar
            current = f"\nRecortada: http://localhost:{self.port}{self.get_key(url)} || Real: {url}\n"
            # Actualizamos page principal
            (code, page) = self.get('/', current)
            return code, page
        else:
            code= "400 Bad Request"
            page= '<html>No se ha introducido ninguna url</html>'
        return code, page


if __name__ == '__main__':
    try:
        app = URLShortener("localhost", 1234)
    except KeyboardInterrupt:
        print("app closed")
